/*!
    \file    gd32f30x_it.c
    \brief   interrupt service routines

    \version 2017-02-10, V1.0.0, firmware for GD32F30x
    \version 2018-10-10, V1.1.0, firmware for GD32F30x
    \version 2018-12-25, V2.0.0, firmware for GD32F30x
    \version 2020-09-30, V2.1.0, firmware for GD32F30x 
*/

/*
    Copyright (c) 2020, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "gd32f30x.h"
#include "DevicesQueue.h"
#include "DevicesUart.h"
#include "DevicesTime.h"
#include "DevicesCi24R1.h"
#include "gd32f30x_it.h"
#include "main.h"
#include "systick.h"

/*!
    \brief      this function handles NMI exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void NMI_Handler(void)
{
}

/*!
    \brief      this function handles HardFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void HardFault_Handler(void)
{
    /* if Hard Fault exception occurs, go to infinite loop */
    while (1){
    }
}

/*!
    \brief      this function handles MemManage exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void MemManage_Handler(void)
{
    /* if Memory Manage exception occurs, go to infinite loop */
    while (1){
    }
}

/*!
    \brief      this function handles BusFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void BusFault_Handler(void)
{
    /* if Bus Fault exception occurs, go to infinite loop */
    while (1){
    }
}

/*!
    \brief      this function handles UsageFault exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void UsageFault_Handler(void)
{
    /* if Usage Fault exception occurs, go to infinite loop */
    while (1){
    }
}

/*!
    \brief      this function handles SVC exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void SVC_Handler(void)
{
}

/*!
    \brief      this function handles DebugMon exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void DebugMon_Handler(void)
{
}

/*!
    \brief      this function handles PendSV exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void PendSV_Handler(void)
{
}

/*!
    \brief      this function handles SysTick exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void SysTick_Handler(void)
{
    delay_decrement();
}

void vUSART0ReceiveCallback(void)
{
    static uint32_t uiMDANdtrOld = 0;
    uint32_t uiMDANdtrNow = 0;

    while(uiMDANdtrOld != (uiMDANdtrNow = USART0_DMA_READ_LENGTH - dma_transfer_number_get(DMA0, DMA_CH4)))
    {
        if(uiMDANdtrNow < uiMDANdtrOld)
        {
            /* 把一帧数据读取到UART队列缓存里 */
            enumQueuePushDatas(&g_TypeQueueUart0Read, &g_USART0ReadDMABuff[uiMDANdtrOld], USART0_DMA_READ_LENGTH - uiMDANdtrOld);

            uiMDANdtrOld = 0;
        }

        /* 把一帧数据读取到UART队列缓存里 */
        enumQueuePushDatas(&g_TypeQueueUart0Read, &g_USART0ReadDMABuff[uiMDANdtrOld], uiMDANdtrNow - uiMDANdtrOld);

        uiMDANdtrOld = uiMDANdtrNow;
    }
}

/*!
    \brief      this function handles USART interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USART0_IRQHandler(void)
{
    volatile static uint32_t uiMDANdtrNow = 0, uiMDANdtrOld = 0;

    if(usart_interrupt_flag_get(USART0, USART_INT_FLAG_IDLE) != RESET)
    {
        vUSART0ReceiveCallback();

        /* 软件先读USART_STAT0，再读USART_DATA可清除空闲中断标志 */
        usart_data_receive(USART0);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_IDLE);
    }
    /* 错误中断 */
    else
    {
        usart_flag_clear(USART0, USART_FLAG_CTS);
        usart_flag_clear(USART0, USART_FLAG_LBD);
        usart_flag_clear(USART0, USART_FLAG_TC);
        usart_flag_clear(USART0, USART_FLAG_RBNE);
        usart_flag_clear(USART0, USART_FLAG_EB);
        usart_flag_clear(USART0, USART_FLAG_RT);

        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_CTS);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_LBD);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_TC);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_RBNE);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_EB);
        usart_interrupt_flag_clear(USART0, USART_INT_FLAG_RT);

        /* 重新初始化 */
        vUart0Init();
    }
}

/*!
    \brief      this function handles DMA interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void DMA0_Channel4_IRQHandler(void)
{
    if(dma_interrupt_flag_get(DMA0, DMA_CH4, DMA_INT_FTF) != RESET)
    {
        vUSART0ReceiveCallback();

        dma_interrupt_flag_clear(DMA0, DMA_CH4, DMA_INT_FTF);
    }
    else if(dma_interrupt_flag_get(DMA0, DMA_CH4, DMA_INT_HTF) != RESET)
    {
        vUSART0ReceiveCallback();

        dma_interrupt_flag_clear(DMA0, DMA_CH4, DMA_INT_HTF);
    }
}

/*!
    \brief      this function handles Exit interrupt request
    \param[in]  none
    \param[out] none
    \retval     none
*/
void EXTI10_15_IRQHandler(void)
{
    if(exti_interrupt_flag_get(EXTI_14) != RESET)
    {
        exti_interrupt_flag_clear(EXTI_14);

        /* 紧急调用接收，防止Ci24R1的接收FIFO溢出 */
        cCi24R1ReceiveDatas();
    }

    if(exti_interrupt_flag_get(EXTI_15) != RESET)
    {
        exti_interrupt_flag_clear(EXTI_15);
    }
}

/*!
    \brief      This function handles TIMER interrupt request.
    \param[in]  none
    \param[out] none
    \retval     none
*/
void TIMER5_IRQHandler(void)
{
    if(timer_interrupt_flag_get(TIMER5, TIMER_INT_UP) != RESET)
    {
        timer_interrupt_flag_clear(TIMER5, TIMER_INT_UP);

        /* 溢出值为65536 */
        g_lTimeBase += 65536;
    }
}
