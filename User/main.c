/*!
    \file    main.c
    \brief   led spark with systick, USART print and key example

    \version 2017-02-10, V1.0.0, firmware for GD32F30x
    \version 2018-10-10, V1.1.0, firmware for GD32F30x
    \version 2018-12-25, V2.0.0, firmware for GD32F30x
    \version 2020-09-30, V2.1.0, firmware for GD32F30x 
*/

/*
    Copyright (c) 2020, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "gd32f30x.h"
#include "systick.h"
#include <stdio.h>

#include "DevicesDelay.h"
#include "DevicesTimer.h"
#include "DevicesUart.h"
#include "DevicesQueue.h"
#include "DevicesCi24R1.h"

#include "main.h"


#define MESSAGE_ANALYSIS_BUFF_LENGTH 512
uint8_t st_ucUartSendBuff[MESSAGE_ANALYSIS_BUFF_LENGTH];
uint8_t st_ucCi24R1SendBuff[MESSAGE_ANALYSIS_BUFF_LENGTH];


/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    int32_t iLength = 0;

    /* enable IRC40K */
    rcu_osci_on(RCU_IRC40K);
    SystemCoreClockUpdate();

    /* 设定中断优先级分组 */
    nvic_priority_group_set(NVIC_PRIGROUP_PRE4_SUB0);

    /* 开启复用时钟 */
    rcu_periph_clock_enable(RCU_AF);

    /* JTAG引脚复用成普通IO */
    gpio_pin_remap_config(GPIO_SWJ_SWDPENABLE_REMAP, ENABLE);

    cTimer5Init();
    vDelayInit();

    /* 初始化透传串口 */
    vUart0Init();
    /* print out the clock frequency of system, AHB, APB1 and APB2 */
    printf("SystemCoreClock: %d\r\n", (int32_t)SystemCoreClock);
    printf("CK_SYS is %d\r\n", rcu_clock_freq_get(CK_SYS));
    printf("CK_AHB is %d\r\n", rcu_clock_freq_get(CK_AHB));
    printf("CK_APB1 is %d\r\n", rcu_clock_freq_get(CK_APB1));
    printf("CK_APB2 is %d\r\n\n", rcu_clock_freq_get(CK_APB2));

    if(RESET != rcu_flag_get(RCU_FLAG_LPRST))
        printf("低功耗复位\r\n");
    if(RESET != rcu_flag_get(RCU_FLAG_WWDGTRST))
        printf("窗口看门狗复位\r\n");
    if(RESET != rcu_flag_get(RCU_FLAG_FWDGTRST))
        printf("独立看门狗复位\r\n");
    if(RESET != rcu_flag_get(RCU_FLAG_SWRST))
        printf("软件复位\r\n");
    if(RESET != rcu_flag_get(RCU_FLAG_PORRST))
        printf("掉电/上电复位\r\n");
    if(RESET != rcu_flag_get(RCU_FLAG_EPRST))
        printf("NRST引脚复位\r\n");
    /* 清除复位标置 */
    rcu_all_reset_flag_clear();

    enumQueueInit();

    /* 初始化无线模块 */
    vCi24R1Init();


    while(1)
    {
        /* 接收无线数据 */
        if(cCi24R1IoRead() == 0)
        {
            cCi24R1ReceiveDatas();
        }

        /* 把接收到的串口0数据，透传给无线 */
        while((iLength = iQueueGetLengthOfOccupy(&g_TypeQueueUart0Read)) > 0)
        {
            iLength = (iLength > MESSAGE_ANALYSIS_BUFF_LENGTH) ? MESSAGE_ANALYSIS_BUFF_LENGTH : iLength;

            enumQueuePopDatas(&g_TypeQueueUart0Read, st_ucCi24R1SendBuff, iLength);

            cCi24R1SendDatas(st_ucCi24R1SendBuff, iLength);
        }

        /* 把接收到的无线数据，透传给串口0 */
        while((iLength = iQueueGetLengthOfOccupy(&g_TypeQueueCi24R1Read)) > 0)
        {
            iLength = (iLength > MESSAGE_ANALYSIS_BUFF_LENGTH) ? MESSAGE_ANALYSIS_BUFF_LENGTH : iLength;

            enumQueuePopDatas(&g_TypeQueueCi24R1Read, st_ucUartSendBuff, iLength);

            vUartDMASendDatas(USART0, st_ucUartSendBuff, iLength);
        }
    }
}

/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    /* 等待发送寄存器发送完成，并且该串口已经在正常工作、已经使能发送功能 */
    while((RESET == usart_flag_get(UART_LOG, USART_FLAG_TBE)) && ((USART_CTL0(UART_LOG) & USART_CTL0_UEN) != 0) && ((USART_CTL0(UART_LOG) & USART_TRANSMIT_ENABLE) != 0));

    /* Transmit Data */
    USART_DATA(UART_LOG) = ch & 0xFF;

    return ch;
}
