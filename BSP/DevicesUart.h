/*
* Author: honrun
*/
#ifndef DEVICESUART_H_
#define DEVICESUART_H_


#define UART_LOG                USART0


#define USART0_DMA_READ_LENGTH  (256)
#define USART0_DMA_SEND_LENGTH  (2048)



extern uint8_t g_USART0ReadDMABuff[USART0_DMA_READ_LENGTH + 4];



void vUart0Init(void);


void vUart0DMAInit(void);


void vUartSendDatas(uint32_t uiUsartPeriph, void *pvDatas, int32_t iLength);
void vUartSendStrings(uint32_t uiUsartPeriph, char *pcStrings);

void vUartDMASendDatas(uint32_t uiUsartPeriph, void *pvDatas, int32_t iLength);
void vUartDMASendStrings(uint32_t uiUsartPeriph, char *pcStrings);


#endif /* DEVICESUART_H_ */
