#include "gd32f30x.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "DevicesDelay.h"
#include "DevicesSoftSPI.h"
#include "DevicesQueue.h"
#include "DevicesCi24R1.h"



int8_t cCi24R1Check(void);
void vCi24R1IoModeSet(uint32_t uiGpioMode, int8_t cInterrupt);
int8_t cCi24R1WriteCmd(uint8_t ucCmd);
int8_t cCi24R1WriteByte(uint8_t ucAddr, uint8_t ucValue);
uint8_t ucCi24R1ReadByte(uint8_t ucAddr);
int8_t cCi24R1WriteDatas(uint8_t ucAddr, void *pvBuff, int32_t iLength);
int8_t cCi24R1ReadDatas(uint8_t ucAddr, void *pvBuff, int32_t iLength);
void cCi24R1TxMode(void);
void cCi24R1RxMode(void);


/* 设备初始化是否成功 */
static int8_t st_cCi24R1InitState = 0;
static int8_t st_cCi24R1Busy = 0;


void vCi24R1Init(void)
{
    vSoftSPI0Init();


    /* 设置DATA接口为SPI模式 */
    cCi24R1WriteCmd(SELSPI);

    /* CE = 0，进入待机模式 */
    cCi24R1WriteCmd(CE_OFF);


    /* 设备检测 */
    if((st_cCi24R1InitState = cCi24R1Check()) != 1)
        return;

    vCi24R1ExitInit();


    /* 通过先设置 reg0F_selH/L 的值，决定寄存器地址（0x0F）的具体映射 */
    cCi24R1WriteByte(W_REGISTER + EN_RXADDR, 0x01);
    cCi24R1WriteByte(W_REGISTER + EN_AA, 0x81);
    /* 设置模拟电容 16.5pf */
    cCi24R1WriteByte(W_REGISTER + 0x0F, 0xB0);


    /* 使能各管道动态负载长度 */
    cCi24R1WriteByte(W_REGISTER + DYNPD, DPL_P0);

    /* 使能动态负载长度 + 使能命令W_TX_PAYLOAD_NOACK */
    cCi24R1WriteByte(W_REGISTER + FEATURE, EN_DPL | EN_DYN_ACK);

    /* 使能各管道自动确认 */
    cCi24R1WriteByte(W_REGISTER + EN_AA, ENAA_P0);

    /* 使能各管道接收地址 */
    cCi24R1WriteByte(W_REGISTER + EN_RXADDR, ERX_P0);

    /* 地址宽度 */
    cCi24R1WriteByte(W_REGISTER + SETUP_AW, AW_5BYTES);

    /* 设置自动重发延时 + 设备自动重发次数（最大15次） */
    cCi24R1WriteByte(W_REGISTER + SETUP_RETR, ARD_250US | ARC_15);

    /* 设置芯片工作时的信道 */
    cCi24R1WriteByte(W_REGISTER + RF_CH, 97);

    /* 设置射频数据率为250kbps + 9dBm发射功率 */
    cCi24R1WriteByte(W_REGISTER + RF_SETUP, RF_SPEED_250kbps | PWR_3dBm);


    /* 接收模式 */
    cCi24R1RxMode();

    /* 设置DATA接口为IRQ输出模式 */
    cCi24R1WriteCmd(SELIRQ);

    /* 配置MCU数据引脚为 GPIO中断模式 */
    vCi24R1IoModeSet(GPIO_MODE_IPU, 1);
}

int8_t cCi24R1Check(void)
{
    const static uint8_t st_ucCi24R1DefAddrs[5] = {0xE7, 0xE7, 0xE7, 0xE7, 0xE7};
    char st_ucCi24R1ReadAddrs[5];

    /* 设置发射地址 */
    cCi24R1WriteDatas(W_REGISTER + TX_ADDR, (void *)st_ucCi24R1DefAddrs, 5);
    /* 设置接收地址 */
    cCi24R1WriteDatas(W_REGISTER + RX_ADDR_P0, (void *)st_ucCi24R1DefAddrs, 5);
    /* 读取发射地址 */
    cCi24R1ReadDatas(R_REGISTER + TX_ADDR, st_ucCi24R1ReadAddrs, 5);

    if(memcmp(st_ucCi24R1ReadAddrs, (void *)st_ucCi24R1DefAddrs, 5) != 0)
        return 0;

    return 1;
}

void vCi24R1ExitInit(void)
{
    rcu_periph_clock_enable(RCU_GPIOB);

    /* configure pin as input */
    gpio_init(GPIOB, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_PIN_14);

    /* enable and set key EXTI interrupt to the lowest priority */
    nvic_irq_disable(EXTI10_15_IRQn);

    /* connect key EXTI line to key GPIO pin */
    gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOB, GPIO_PIN_SOURCE_14);

    exti_interrupt_flag_clear(EXTI_14);

    /* configure key EXTI line */
    exti_init(EXTI_14, EXTI_INTERRUPT, EXTI_TRIG_FALLING);
}

void vCi24R1IoModeSet(uint32_t uiGpioMode, int8_t cInterrupt)
{
    gpio_init(SOFT_SPI0_MOSI_GPIO_Port, uiGpioMode, GPIO_OSPEED_50MHZ, SOFT_SPI0_MOSI_Pin);

    if(cInterrupt == 0)
    {
        /* disable interrupt */
        nvic_irq_disable(EXTI10_15_IRQn);
    }
    else
    {
        exti_interrupt_flag_clear(EXTI_14);
        /* enable interrupt */
        nvic_irq_enable(EXTI10_15_IRQn, 10U, 0U);
    }
}

int8_t cCi24R1IoRead(void)
{
    return gpio_input_bit_get(SOFT_SPI0_MOSI_GPIO_Port, SOFT_SPI0_MOSI_Pin);
}

int8_t cCi24R1WriteCmd(uint8_t ucCmd)
{
    SOFT_SPI0_CS_LOW();

    vSoftSPIxWriteByte(ucCmd);

    SOFT_SPI0_CS_HIGH();

    return 0;
}

int8_t cCi24R1WriteByte(uint8_t ucAddr, uint8_t ucValue)
{
    SOFT_SPI0_CS_LOW();

    vSoftSPIxWriteByte(ucAddr);
    vSoftSPIxWriteByte(ucValue);

    SOFT_SPI0_CS_HIGH();

    return 0;
}

uint8_t ucCi24R1ReadByte(uint8_t ucAddr)
{
    uint8_t ucValue = 0;

    SOFT_SPI0_CS_LOW();

    vSoftSPIxWriteByte(ucAddr);
    ucValue = ucSoftSPIxReadByte();

    SOFT_SPI0_CS_HIGH();

    return ucValue;
}

int8_t cCi24R1WriteDatas(uint8_t ucAddr, void *pvBuff, int32_t iLength)
{
    int8_t cError = 0;

    SOFT_SPI0_CS_LOW();

    vSoftSPIxWriteByte(ucAddr);
    cError = cSoftSPIxWriteDatas(pvBuff, iLength);

    SOFT_SPI0_CS_HIGH();

    return cError;
}

int8_t cCi24R1ReadDatas(uint8_t ucAddr, void *pvBuff, int32_t iLength)
{
    int8_t cError = 0;

    SOFT_SPI0_CS_LOW();

    vSoftSPIxWriteByte(ucAddr);
    cError = cSoftSPIxReadDatas(pvBuff, iLength);

    SOFT_SPI0_CS_HIGH();

    return cError;
}

void cCi24R1TxMode(void)
{
    /* 设置DATA接口为SPI模式 */
    cCi24R1WriteCmd(SELSPI);

    /* CE = 0，返回待机模式 */
    cCi24R1WriteCmd(CE_OFF);

    vDelayMs(5);

    /* 开启接收中断、发射中断、最大重发计数中断 + 使能CRC + 1Byte CRC + 进入待机模式 + 发射模式 */
    cCi24R1WriteByte(W_REGISTER + CONFIG, EN_CRC | CRCO | PWR_UP);

    /* 清空TX FIFO */
    cCi24R1WriteCmd(FLUSH_TX);

    /* CE = 1，进入发送空闲模式 */
    cCi24R1WriteCmd(CE_ON);
}

void cCi24R1RxMode(void)
{
    /* 设置DATA接口为SPI模式 */
    cCi24R1WriteCmd(SELSPI);

    /* CE = 0，返回待机模式 */
    cCi24R1WriteCmd(CE_OFF);

    vDelayMs(5);

    /* 使能各管道接收地址 */
    cCi24R1WriteByte(W_REGISTER + EN_RXADDR, ERX_P0);

    /* 开启接收中断、发射中断、最大重发计数中断 + 使能CRC + 1Byte CRC + 进入待机模式 + 接收模式 */
    cCi24R1WriteByte(W_REGISTER + CONFIG, EN_CRC | CRCO | PWR_UP | PRIM_RX);

    /* 清空RX FIFO */
    cCi24R1WriteCmd(FLUSH_RX);

    /* CE = 1，进入接收模式 */
    cCi24R1WriteCmd(CE_ON);
}

static int8_t cCi24R1SendFrame(void *pvBuff, int32_t iLength)
{
    uint8_t ucState = 0;

    /* 读取发射FIFO是否已满 */
    while((ucState = ucCi24R1ReadByte(R_REGISTER + FIFO_STATUS)) & TX_FULL_1);

    /* 写入数据到FIFO */
    cCi24R1WriteDatas(W_TX_PAYLOAD_NOACK, pvBuff, iLength);

    /* 设置DATA接口为IRQ输出模式 */
    cCi24R1WriteCmd(SELIRQ);

    /* 配置MCU数据引脚为 GPIO中断模式 */
    vCi24R1IoModeSet(GPIO_MODE_IPU, 0);
    vDelayMs(1);

    /* 发射完成或者发射错误，MISO引脚会自动拉低 */
    while(cCi24R1IoRead() != 0);

    /* 配置MCU数据引脚为 SPI模式 */
    vCi24R1IoModeSet(GPIO_MODE_OUT_PP, 0);

    /* 设置DATA接口为SPI模式 */
    cCi24R1WriteCmd(SELSPI);

    /* 读取中断标志 */
    ucState = ucCi24R1ReadByte(R_REGISTER + STATUS);

    /* 清除中断标志 */
    cCi24R1WriteByte(W_REGISTER + STATUS, ucState);

    return 0;
}
 int8_t cCi24R1SendDatas(void *pvBuff, int32_t iLength)
{
    int32_t iLengthTemp = 0;
    uint8_t *pucBuff = pvBuff;

    if(st_cCi24R1InitState == 0)
        return 1;

    if((pvBuff == NULL) || (iLength < 1))
        return 2;

    if(st_cCi24R1Busy != 0)
        return 3;

    st_cCi24R1Busy = 1;

    /* 配置MCU数据引脚为 SPI模式 */
    vCi24R1IoModeSet(GPIO_MODE_OUT_PP, 0);

    /* 设置为发射模式 */
    cCi24R1TxMode();

    /* 向FIFO填充数据 */
    while(iLength > 0)
    {
        /* 每个FIFO最大的长度为32Byte */
        iLengthTemp = iLength > 32 ? 32 : iLength;

        /* 写入数据到FIFO */
        cCi24R1SendFrame(pucBuff, iLengthTemp);

        pucBuff += iLengthTemp;
        iLength -= iLengthTemp;
    }

    /* 设置为接收模式 */
    cCi24R1RxMode();

    /* 设置DATA接口为IRQ输出模式 */
    cCi24R1WriteCmd(SELIRQ);

    /* 配置MCU数据引脚为 GPIO中断模式 */
    vCi24R1IoModeSet(GPIO_MODE_IPU, 1);

    st_cCi24R1Busy = 0;

    return 0;
}

/* 每次读取1个FIFO的数据 */
int8_t cCi24R1ReceiveDatas(void)
{
    uint8_t ucState = 0, ucBuff[32];
    int8_t cError = 0;

    if(st_cCi24R1InitState == 0)
        return 1;

    if(st_cCi24R1Busy != 0)
        return 2;

    st_cCi24R1Busy = 1;

    /* 配置MCU数据引脚为 SPI模式 */
    vCi24R1IoModeSet(GPIO_MODE_OUT_PP, 0);

    /* 设置DATA接口为SPI模式 */
    cCi24R1WriteCmd(SELSPI);

    while((ucCi24R1ReadByte(R_REGISTER + FIFO_STATUS) & RX_EMPTY) == 0)
    {
        /* 读取接收到的字节数 */
        ucState = ucCi24R1ReadByte(R_RX_PL_WID);

        /* 从FIFO读取数据 */
        cError = cCi24R1ReadDatas(R_RX_PAYLOAD, ucBuff, ucState);

        /* 把收到的数据存放到 队列 */
        enumQueuePushDatas(&g_TypeQueueCi24R1Read, ucBuff, ucState);
    }

    /* 读取中断标志 */
    ucState = ucCi24R1ReadByte(R_REGISTER + STATUS);

    /* 清除中断标志 */
    cCi24R1WriteByte(W_REGISTER + STATUS, ucState);

    /* 设置DATA接口为IRQ输出模式 */
    cCi24R1WriteCmd(SELIRQ);

    /* 配置MCU数据引脚为 GPIO中断模式 */
    vCi24R1IoModeSet(GPIO_MODE_IPU, 1);

    st_cCi24R1Busy = 0;

    return cError;
}
