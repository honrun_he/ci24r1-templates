/*
* Author: honrun
*/
#ifndef _DevicesTime_H_
#define _DevicesTime_H_

#include <stdint.h>



/* 闰年 */
#ifndef USER_LEAP_YEAR
#define USER_LEAP_YEAR   1
#endif // LEAP_YEAR

/* 平年 */
#ifndef USER_COMMON_YEAR
#define USER_COMMON_YEAR 0
#endif // COMMON_YEAR


/* 判断是否为闰年 */
#define YEAR_LEAP(year) ((((year) % 4)      != 0) ? USER_COMMON_YEAR : \
                         (((year) % 100)    != 0) ? USER_LEAP_YEAR   : \
                         (((year) % 400)    != 0) ? USER_COMMON_YEAR : \
                         (((year) % 3200)   != 0) ? USER_LEAP_YEAR   : \
                         (((year) % 172800) != 0) ? USER_COMMON_YEAR : USER_LEAP_YEAR)
/* 获取年份天数 */
#define DAYS_OF_THE_YEAR(year) (YEAR_LEAP(year) == USER_LEAP_YEAR ? 366 : 365)
/* 获取月份天数 */
#define DAYS_OF_THE_MONTH(year, month) ((((month) == 2) & (YEAR_LEAP(year) == USER_LEAP_YEAR)) ? 29 : st_ucMonthDays[(month) - 1])



typedef struct{
    int32_t year;
    int8_t month;
    int8_t day;
    int8_t hour;
    int8_t minute;
    int8_t second;
    int8_t week;
    int8_t UTC;
}TimeType;



/* 系统计时时基，不可更改 */
extern int64_t g_lTimeBase;


void vStampToTime(int64_t lStamp, TimeType *ptypeTime, int8_t cUTC);
int64_t lTimeToStamp(TimeType *ptypeTime);
int64_t lTimeGetStamp(void);
void vRealTimeUTCSet(int8_t cUTC);
int8_t cRealTimeUTCGet(void);

void vTimestampSet(int64_t lUNIXTimeStamp);
int64_t lTimestampGet(void);

char *pcStampToDateTimeStrings(int64_t lStamp);


#endif
