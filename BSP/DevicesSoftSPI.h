#ifndef _DevicesSoftSPI_H_
#define _DevicesSoftSPI_H_



#define SOFT_SPI0_CS_GPIO_Port      GPIOB
#define SOFT_SPI0_CS_Pin            GPIO_PIN_12

#define SOFT_SPI0_SCLK_GPIO_Port    GPIOB
#define SOFT_SPI0_SCLK_Pin          GPIO_PIN_13

#define SOFT_SPI0_MISO_GPIO_Port    GPIOB
#define SOFT_SPI0_MISO_Pin          GPIO_PIN_14

#define SOFT_SPI0_MOSI_GPIO_Port    GPIOB
#define SOFT_SPI0_MOSI_Pin          GPIO_PIN_14



#define SOFT_SPI0_CS_HIGH()         gpio_bit_set(SOFT_SPI0_CS_GPIO_Port, SOFT_SPI0_CS_Pin)
#define SOFT_SPI0_CS_LOW()          gpio_bit_reset(SOFT_SPI0_CS_GPIO_Port, SOFT_SPI0_CS_Pin)

#define SOFT_SPI0_SCLK_HIGH()       gpio_bit_set(SOFT_SPI0_SCLK_GPIO_Port, SOFT_SPI0_SCLK_Pin)
#define SOFT_SPI0_SCLK_LOW()        gpio_bit_reset(SOFT_SPI0_SCLK_GPIO_Port, SOFT_SPI0_SCLK_Pin)

#define SOFT_SPI0_MISO_HIGH()       gpio_bit_set(SOFT_SPI0_MISO_GPIO_Port, SOFT_SPI0_MISO_Pin)
#define SOFT_SPI0_MISO_LOW()        gpio_bit_reset(SOFT_SPI0_MISO_GPIO_Port, SOFT_SPI0_MISO_Pin)

#define SOFT_SPI0_MOSI_HIGH()       gpio_bit_set(SOFT_SPI0_MOSI_GPIO_Port, SOFT_SPI0_MOSI_Pin)
#define SOFT_SPI0_MOSI_LOW()        gpio_bit_reset(SOFT_SPI0_MOSI_GPIO_Port, SOFT_SPI0_MOSI_Pin)

#define SOFT_SPI0_MISO_READ()       gpio_input_bit_get(SOFT_SPI0_MISO_GPIO_Port, SOFT_SPI0_MISO_Pin)




void vSoftSPI0Init(void);
void vSoftSPIxWriteByte(uint8_t ucByte);
uint8_t ucSoftSPIxReadByte(void);
int8_t cSoftSPIxWriteDatas(void *pvBuff, int32_t iLength);
int8_t cSoftSPIxReadDatas(void *pvBuff, int32_t iLength);


#endif
