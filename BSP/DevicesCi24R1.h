#ifndef _DevicesCi24R1_H_
#define _DevicesCi24R1_H_


/* Ci24R1与Ci24R1共用指令 */
#define R_REGISTER          0x00    /* 读寄存器命令（低5bit为寄存器地址） */
#define W_REGISTER          0x20    /* 写寄存器命令（低5bit为寄存器地址） */
#define R_RX_PAYLOAD        0x61    /* 从FIFO中读收到的数据，1-32字节，读出后FIFO数据被删除。适用于接收模式 */
#define W_TX_PAYLOAD        0xA0    /* 写发射负载数据，大小为1-32字节，适用于发射模式 */
#define FLUSH_TX            0xE1    /* 清空TX FIFO，适用于发射模式 */
#define FLUSH_RX            0xE2    /* 清空RX FIFO，适用于接收模式。如果需要回ACK，则不能在回ACK操作完成前进行清空FIFO，否则视为通信失败 */
#define REUSE_TX_PL         0xE3    /* 适用于发送方，清空TX FIFO或对FIFO写入新的数据后不能使用该命令 */
#define R_RX_PL_WID         0x60    /* 读取收到的数据字节数 */
#define W_ACK_PAYLOAD       0xA8    /* 适用于接收方，通过PIPE PPP将数据通过ACK的形式发出去，最多允许三帧数据存于FIFO中 */
#define W_TX_PAYLOAD_NOACK  0xB0    /* 适用于发射模式，使用这个命令同时需要将AUTOACK位置1 */
#define NOP                 0xFF    /* 无操作。可用于返回STATUS值 */

/* Ci24R1独占指令 */
#define CE_ON               0x70    /* 打开CE CE=1,CE_STATE=1 */
#define CE_OFF              0x71    /* 关闭CE CE=0,CE_STATE=0 */
#define SELSPI              0x74    /* 选择DATA引脚为SPI功能 */
#define SELIRQ              0x75    /* 选择DATA引脚输出IRQ值 */


/* 寄存器地址 */
#define CONFIG              0x00    /* 配置寄存器 */
#define EN_AA               0x01    /* 使能接收管道自动确认 */
#define EN_RXADDR           0x02    /* 使能接收数据管道地址 */
#define SETUP_AW            0x03    /* 地址宽度配置 */
#define SETUP_RETR          0x04    /* 自动重发配置 */
#define RF_CH               0x05    /* 设置芯片工作时的信道，分别对应第0~125个信道；信道间隔为1MHz，默认为02即2402MHz */
#define RF_SETUP            0x06    /* 射频配置 */
#define STATUS              0x07    /* 状态寄存器 */
#define OBSERVE_TX          0x08    /* 发射结果统计 */
#define RSSI                0x09    /* 接收信号强度检测 */
#define RX_ADDR_P0          0x0A    /* 数据管道0的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define RX_ADDR_P1          0x0B    /* 数据管道1的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define RX_ADDR_P2          0x0C    /* 数据管道2的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define RX_ADDR_P3          0x0D    /* 数据管道3的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define RX_ADDR_P4          0x0E    /* 数据管道4的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define RX_ADDR_P5          0x0F    /* 数据管道5的接收地址，最大宽度为5bytes (LSByte最先写入，通过SETUP_AW配置地址宽度) */
#define TX_ADDR             0x10    /* 发射方的发射地址(LSByte最先写入)，如果发射放需要收ACK确认信号，则需要配置RX_ADDR_P0的值等于TX_ADDR，并使能ARQ */
#define RX_PW_P0            0x11    /* 接收数据管道0数据字节数 */
#define RX_PW_P1            0x12    /* 接收数据管道1数据字节数 */
#define RX_PW_P2            0x13    /* 接收数据管道2数据字节数 */
#define RX_PW_P3            0x14    /* 接收数据管道3数据字节数 */
#define RX_PW_P4            0x15    /* 接收数据管道4数据字节数 */
#define RX_PW_P5            0x16    /* 接收数据管道5数据字节数 */
#define FIFO_STATUS         0x17    /* FIFO状态 */
#define BandWidth_TX        0x18
#define DYNPD               0x1C    /* 使能各管道动态负载长度 */
#define FEATURE             0x1D    /* 特征寄存器 */


/* 各寄存器bit定义 */
/* 配置寄存器配置 */
#define MASK_RX_DR          (1 << 6)    /* 接收中断屏蔽控制（0：接收中断使能，RX_DR中断标志在IRQ引脚上产生中断信号，低电平有效1：接收中断关闭，RX_DR中断标志不影响IRQ引脚输出） */
#define MASK_TX_DS          (1 << 5)    /* 发射中断屏蔽控制（0：发射中断使能，TX_DS中断标志在IRQ引脚上产生中断信号，低电平有效1：发射中断关闭，TX_DS中断标志不影响IRQ引脚输出） */
#define MASK_MAX_RT         (1 << 4)    /* 最大重发计数中断屏蔽控制（0：最大重发计数中断使能，MAX_RT中断标志在IRQ引脚上产生中断信号，低电平有效1：最大重发计数中断关闭，MAX_RT中断标志不影响IRQ引脚输出） */
#define EN_CRC              (1 << 3)    /* 使能CRC。如果EN_AA不全为零时，EN_CRC必须为1。 */
#define CRCO                (1 << 2)    /* CRC长度配置（0：1Byte、1：2Byte） */
#define PWR_UP              (1 << 1)    /* 关断/开机模式配置（0：关断模式、1：开机模式） */
#define PRIM_RX             (1 << 0)    /* 发射/接收配置，只能在Shutdown和Standby下更改（0：发射模式、1：接收模式） */

/* 使能接收管道自动确认 */
#define ENAA_P5             (1 << 5)
#define ENAA_P4             (1 << 4)
#define ENAA_P3             (1 << 3)
#define ENAA_P2             (1 << 2)
#define ENAA_P1             (1 << 1)
#define ENAA_P0             (1 << 0)

/* 使能接收管道地址 */
#define ERX_P5              (1 << 5)
#define ERX_P4              (1 << 4)
#define ERX_P3              (1 << 3)
#define ERX_P2              (1 << 2)
#define ERX_P1              (1 << 1)
#define ERX_P0              (1 << 0)

/* 发射方/接收方地址宽度 */
#define AW_RERSERVED        0
#define AW_3BYTES           1
#define AW_4BYTES           2
#define AW_5BYTES           3

/* 自动重发延时配置 */
#define ARD_250US           (0 << 4)
#define ARD_500US           (1 << 4)
#define ARD_750US           (2 << 4)
#define ARD_1000US          (3 << 4)
#define ARD_2000US          (7 << 4)
#define ARD_4000US          (15 << 4)
/* 自动重发次数 */
#define ARC_DISABLE         0
#define ARC_15              15

/* 射频配置 */
#define CONT_WAVE           (1 << 7)    /* 使能恒载波发射模式，用来测试发射功率 */
#define RF_SPEED_250kbps    (0x04 << 3) /* 与RF_DR_HIGH共同控制，设置射频数据率为250kbps 、1Mbps或2Mbps */
#define RF_SPEED_2Mbps      (0x01 << 3) /* 与RF_DR_HIGH共同控制，设置射频数据率为250kbps 、1Mbps或2Mbps */
#define RF_SPEED_1Mbps      (0x00 << 3) /* 与RF_DR_HIGH共同控制，设置射频数据率为250kbps 、1Mbps或2Mbps */
/* 设置Tx发射功率 */
#define PWR_11dBm           (7)
#define PWR_10dBm           (6)
#define PWR_9dBm            (5)
#define PWR_7dBm            (4)
#define PWR_3dBm            (3)
#define PWR__1dBm           (2)
#define PWR__4dBm           (1)
#define PWR__9dBm           (0)

/* 状态寄存器 */
#define RX_DR               (1 << 6)    /* RX FIFO有值标志位，写’1’清除 */
#define TX_DS               (1 << 5)    /* 发射端发射完成中断位，如果是ACK模式，则收到ACK确认信号后TX_DS位置’1’，写’1’清除 */
#define MAX_RT              (1 << 4)    /* 达到最大重发次数中断位，写’1’清除 */
#define RX_P_NO             (7 << 1)    /* 收到数据的接收管道PPP号，可以通过SPI读出（000-101：数据管道0-5、110：不可用、111：RX FIFO为空） */
#define TX_FULL             (1 << 0)    /* TX FIFO是否已满 */

/* FIFO状态 */
#define TX_REUSE            (1 << 6)    /* 只用于发射端，FIFO数据重新利用当用REUSE_TX_PL命令后，发射上次已成功发射的数据，通过W_TX_PAYLOAD或FLUSH TX命令关闭该功能 */
#define TX_FULL_1           (1 << 5)    /* TX FIFO满标志（1：TX FIFO满、0：TX FIFO可写） */
#define TX_EMPTY            (1 << 4)    /* TX FIFO空标志（1：TX FIFO空、0：TX FIFO有数据） */
//bit3-bit2, reserved, only '00'
#define RX_FULL             (1 << 1)    /* RX FIFO满标志（1：RX FIFO满、0：RX FIFO可写） */
#define RX_EMPTY            (1 << 0)    /* RX FIFO空标志（1：RX FIFO空、0：RX FIFO有数据） */

/* 使能动态负载长度 */
#define DPL_P5              (1 << 5)
#define DPL_P4              (1 << 4)
#define DPL_P3              (1 << 3)
#define DPL_P2              (1 << 2)
#define DPL_P1              (1 << 1)
#define DPL_P0              (1 << 0)

/* 特征寄存器 */
#define EN_DPL              (1 << 2)    /* 使能动态负载长度 */
#define EN_ACK_PAY          (1 << 1)    /* 使能ACK负载(带负载数据的ACK包) */
#define EN_DYN_ACK          (1 << 0)    /* 使能命令W_TX_PAYLOAD_NOACK */
#define IRQ_ALL             ( RX_DR | TX_DS | MAX_RT )



void vCi24R1Init(void);
void vCi24R1ExitInit(void);
int8_t cCi24R1IoRead(void);

int8_t cCi24R1SendDatas(void *pvBuff, int32_t iLength);
int8_t cCi24R1ReceiveDatas(void);


#endif
