/*
 * DevicesLed.h
 *
 *      Author: Honrun
 */

#ifndef BSP_DEVICESLED_H_
#define BSP_DEVICESLED_H_



/* LED调频频率 */
#define LED_FREQUENCY               2048
/* LED溢出值 */
#define LED_PERIOD                  (SystemCoreClock / LED_FREQUENCY)

/* LED状态机执行周期（ms） */
#define LED_EXECUTION_PERIOD    20
/* 快闪周期（ms） */
#define LED_FLASH_FAST_PERIOD   800
/* 慢闪周期（ms） */
#define LED_FLASH_SLOW_PERIOD   2400
/* SOS模式，周期间隔时间（ms） */
#define LED_SOS_DELAY_PERIOD    2000


/* 驱动模式 */
#define LED_DRIVE_NULL  0
#define LED_DRIVE_IO    1
#define LED_DRIVE_PWM   2
/* 亮度等级 */
#define LED_HIGH_DUTY   1.0f
#define LED_DIM_DUTY    0.5f



#define LED_BLUE_GPIO_Port  GPIOB
#define LED_BLUE_Pin        GPIO_PIN_2



typedef enum {
    LED_CHANNEL_BLUE    = 0x01,
    LED_CHANNEL_ALL     = 0x7FFFFFFF,   /* 全部通道 */
} LedChannelEnum;


typedef enum {
    LED_DISABLE                     = 0, /* 关闭 */
    LED_ENABLE                      = 1, /* 常亮 - 高亮*/
    LED_ENABLE_LOW                  = 3, /* 常亮 - 低亮 */
    LED_DUTY                        = 4, /* 固定占空比 */

    LED_FLASH_FAST                  = 10, /* 快速频率（1Hz） */
    LED_FLASH_FAST_ENABLE_CNT       = 11, /* 闪烁N次后 常亮 */
    LED_FLASH_FAST_DISABLE_CNT      = 12, /* 闪烁N次后 关闭 */

    LED_FLASH_SLOW                  = 20, /* 低速频率（2Hz） */
    LED_FLASH_SLOW_ENABLE_CNT       = 21, /* 闪烁N次后 常亮 */
    LED_FLASH_SLOW_DISABLE_CNT      = 22, /* 闪烁N次后 关闭 */

    LED_BREATHE                     = 30, /* 呼吸 */

    LED_FLASH_SOS                   = 40, /* SOS（2Hz） */

    LED_IDLE                        = 0xFF,
} LedStateEnum;



typedef struct{
    LedStateEnum state; /* 状态 */
    int16_t flashCnt;   /* 闪烁次数 */
    int8_t duty;        /* 占空比 */

    uint8_t driveMode;  /* 驱动模式，0：普通IO；1：PWM */
    uint32_t periph;
    uint32_t channel;
}LedType;


typedef struct{
    LedType blue;
}LedInfoType;




void vLedInit(void);
void vLedOpen(uint32_t uiChannel);
void vLedClose(uint32_t uiChannel);
void vLedRevesal(uint32_t uiChannel);

void vLedMachine(void);
void vLedSetStatus(LedChannelEnum usChannel, LedStateEnum enumStatus, uint8_t ucFlashCnt_or_Duty);
#define vLedSetStatusFlashFast(usChannel) vLedSetStatus((usChannel), LED_FLASH_FAST, 0)
#define vLedSetStatusFlashSlow(usChannel) vLedSetStatus((usChannel), LED_FLASH_SLOW, 0)
#define vLedSetStatusBreathe(usChannel) vLedSetStatus((usChannel), LED_BREATHE, 0)
#define vLedSetStatusDisable(usChannel) vLedSetStatus((usChannel), LED_DISABLE, 0)
LedInfoType *ptypeLedGetInfo(void);



#endif /* BSP_DEVICESLED_H_ */
