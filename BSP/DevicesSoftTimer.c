/*
 *  author: Honrun
 */
#include "stdint.h"
#include "stdio.h"
#include "gd32f30x.h"
#include "DevicesTime.h"
#include "DevicesSoftTimer.h"


static int64_t lSoftTimerGetNow(void);


int8_t cSoftTimerSet(SoftTimerTypeDef *ptypeTimer, int64_t lTime, SoftTimerStateEnum state)
{
    if(ptypeTimer == NULL)
        return -1;

    ptypeTimer->timeStop = lSoftTimerGetNow() + lTime;
    ptypeTimer->timeDuration = lTime;
    cSoftTimerSetState(ptypeTimer, state);

    return 0;
}

int8_t cSoftTimerReset(SoftTimerTypeDef *ptypeTimer)
{
    if(ptypeTimer == NULL)
        return -1;

    cSoftTimerSet(ptypeTimer, ptypeTimer->timeDuration, softTimerOpen);

    return 0;
}

int8_t cSoftTimerReload(SoftTimerTypeDef *ptypeTimer)
{
    if(ptypeTimer == NULL)
        return -1;

    ptypeTimer->timeStop += ptypeTimer->timeDuration;
    ptypeTimer->state = softTimerOpen;

    return 0;
}

int8_t cSoftTimerOpen(SoftTimerTypeDef *ptypeTimer)
{
    if(ptypeTimer == NULL)
        return -1;

    ptypeTimer->state = softTimerOpen;

    return 0;
}

int8_t cSoftTimerClose(SoftTimerTypeDef *ptypeTimer)
{
    if(ptypeTimer == NULL)
        return -1;

    ptypeTimer->state = ~softTimerOpen;

    return 0;
}

int8_t cSoftTimerSetState(SoftTimerTypeDef *ptypeTimer, SoftTimerStateEnum enumState)
{
    if(ptypeTimer == NULL)
        return -1;

    ptypeTimer->state = enumState;

    /* 设置为本次立即触发 */
    if(enumState & softTimerOver)
        ptypeTimer->timeStop = lSoftTimerGetNow();

    return 0;
}

SoftTimerStateEnum enumSoftTimerGetState(SoftTimerTypeDef *ptypeTimer)
{
    int64_t ulTimeNow = 0;

    if(ptypeTimer == NULL)
        return softTimerError;

    if((ptypeTimer->state & softTimerOpen) == 0)
        return softTimerClose;

    ulTimeNow = lSoftTimerGetNow();

    if(ulTimeNow >= (ptypeTimer->timeStop))
        return softTimerOver;

    return softTimerOpen;
}

/* 单位us */
static int64_t lSoftTimerGetNow(void)
{
    int64_t lTimeStamp = 0;

    lTimeStamp = lTimeGetStamp();

    return lTimeStamp;
}
