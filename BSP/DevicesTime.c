/*
* Author: honrun
*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "gd32f30x.h"
#include "DevicesTimer.h"
#include "DevicesTime.h"


/* 系统计时时基，不可更改 */
int64_t g_lTimeBase = 0;
/* 实时时钟，可以更改 */
int64_t g_lTimestamp = 0;
/* 时区：默认东八区 */
int8_t g_cRealTimeUTC = 8;

const static uint8_t st_ucMonthDays[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


/* 蔡勒公式计算星期几 Christian Zeller */
uint8_t cTimeToWeek(int32_t iYear, uint8_t ucMonth, uint8_t ucDay)
{
    int32_t iCentury = 0;
    int8_t cWeek = 0;

    /* 如果是1、2月份，则需要把月份当作是上年度的13、14月份 */
    if(ucMonth <= 2)
    {
        iYear -= 1;
        ucMonth += 12;
    }

    iCentury = iYear / 100;
    iYear = iYear % 100;

    cWeek = ((iCentury / 4) - (iCentury * 2) + iYear + (iYear / 4) + (13 * (ucMonth + 1) / 5) + ucDay - 1) % 7;
    cWeek = (cWeek < 0) ? (cWeek + 7) : cWeek;

    return (uint8_t)cWeek;
}

/*
* Return:      void
* Parameters:  lStamp: UNIX时间戳; ptypeTime:时间结构体指针; cUTC: 时区（东时区为正、西时区为负）
* Description: 把UNIX时间戳转换成时间结构体
*/
void vStampToTime(int64_t lStamp, TimeType *ptypeTime, int8_t cUTC)
{
    int32_t lYearTemp = 0;
    int8_t cMonthTemp = 0;

    /* 加入时区值 */
    lStamp += cUTC * 3600;
    ptypeTime->UTC = cUTC;

    if(lStamp >= 0)
    {
        /* 计算全部年份的整年天数（1天有86400秒） */
        for(lYearTemp = 1969; lStamp >= 0; lStamp -= DAYS_OF_THE_YEAR(lYearTemp) * 86400)
            ++lYearTemp;

        /* 计算当前年份下全部月份的整月天数 */
        for(cMonthTemp = 0, lStamp += DAYS_OF_THE_YEAR(lYearTemp) * 86400; lStamp >= 0; lStamp -= DAYS_OF_THE_MONTH(lYearTemp, cMonthTemp) * 86400)
            ++cMonthTemp;

        /* 加上上面循环多减去的1个月 */
        lStamp += DAYS_OF_THE_MONTH(lYearTemp, cMonthTemp) * 86400;
    }
    else
    {
        /* 计算全部年份的整年天数（1天有86400秒） */
        for(lYearTemp = 1970; lStamp < 0; lStamp += DAYS_OF_THE_YEAR(lYearTemp) * 86400)
            --lYearTemp;

        /* 计算当前年份下全部月份的整月天数 */
        for(cMonthTemp = 13, lStamp -= DAYS_OF_THE_YEAR(lYearTemp) * 86400; lStamp < 0; lStamp += DAYS_OF_THE_MONTH(lYearTemp, cMonthTemp) * 86400)
            --cMonthTemp;
    }

    ptypeTime->year = lYearTemp;

    ptypeTime->month = cMonthTemp;

    /* 天从1开始计数 */
    ptypeTime->day = lStamp / 86400 + 1;

    lStamp = (lStamp % 86400) + 86400;

    ptypeTime->hour = lStamp / 3600 % 24;

    ptypeTime->minute = lStamp / 60 % 60;

    ptypeTime->second = lStamp % 60;

    ptypeTime->week = cTimeToWeek(ptypeTime->year, ptypeTime->month, ptypeTime->day);
}

/*
* Return:      UNIX时间戳
* Parameters:  ptypeTime:时间结构体;
* Description: 把时间结构体转换成UNIX时间戳
*/
int64_t lTimeToStamp(TimeType *ptypeTime)
{
    int64_t lDaysNumber = 0, lStamp = 0;
    int32_t lYearTemp = 0;
    int8_t cMonthTemp = 0;

    if(ptypeTime->year >= 1970)
    {
        for(lYearTemp = 1970; lYearTemp < ptypeTime->year; ++lYearTemp)
            lDaysNumber += DAYS_OF_THE_YEAR(lYearTemp);
    }
    else
    {
        for(lYearTemp = 1969; lYearTemp >= ptypeTime->year; --lYearTemp)
            lDaysNumber -= DAYS_OF_THE_YEAR(lYearTemp);
    }

    for(cMonthTemp = 1; cMonthTemp < ptypeTime->month; ++cMonthTemp)
        lDaysNumber += DAYS_OF_THE_MONTH(ptypeTime->year, cMonthTemp);

    lDaysNumber += ptypeTime->day - 1;

    lStamp  = lDaysNumber * 86400;
    lStamp += ptypeTime->hour * 3600;
    lStamp += ptypeTime->minute * 60;
    lStamp += ptypeTime->second;

    /* 加入计算时区值 */
    lStamp -= ptypeTime->UTC * 3600;

    return lStamp;
}

/* 获取系统运行时间：单位us */
int64_t lTimeGetStamp(void)
{
    volatile int64_t lUNIXTimeStamp = 0, lTimeHighFirst = 0, lTimeHighSecond = 1;
    volatile uint32_t uiTimeLow = 0;

    /* 保证不会读取到进位时的值，以造成时间错乱 */
    while(lTimeHighFirst != lTimeHighSecond)
    {
        lTimeHighFirst = g_lTimeBase;

        uiTimeLow = TIMER_CNT(TIMER5);

        lTimeHighSecond = g_lTimeBase;
    }

    lUNIXTimeStamp = lTimeHighFirst + uiTimeLow;

    return lUNIXTimeStamp;
}

/* 设置时间戳：单位us */
void vTimestampSet(int64_t lUNIXTimeStamp)
{
    g_lTimestamp = lUNIXTimeStamp - g_lTimeBase;
}

/* 获取时间戳：单位us */
int64_t lTimestampGet(void)
{
    return g_lTimestamp + g_lTimeBase;
}

/* 时区设置 */
void vRealTimeUTCSet(int8_t cUTC)
{
    g_cRealTimeUTC = cUTC;
}

/* 时区获取 */
int8_t cRealTimeUTCGet(void)
{
    return g_cRealTimeUTC;
}

char *pcStampToDateTimeStrings(int64_t lStamp)
{
    TimeType typeTime = {0};
    static char cDateStrings[32] = {0};

    /* 东8区 */
    vStampToTime(lStamp, &typeTime, 8);

    memset(cDateStrings, 0, sizeof(cDateStrings));
    sprintf(cDateStrings, "%d/%02d/%02d - %02d:%02d:%02d", (int)typeTime.year, typeTime.month, typeTime.day, typeTime.hour, typeTime.minute, typeTime.second);

    return cDateStrings;
}
